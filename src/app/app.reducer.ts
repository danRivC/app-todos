import { Todo } from './todos/models/model';
import { ActionReducerMap } from '@ngrx/store';
import { todoReducer } from './todos/todo.reducer';
import { filtrosValidos } from './filtro/filtro.actions';
import { filtroReducer } from './filtro/filtro.reducer';


export interface AppState {
    todos: Todo[],
    filtro: filtrosValidos
}

//Objeto que se encarga de encapsular todos los reducers 
export const appReducers: ActionReducerMap<AppState> = {
    todos : todoReducer,
    filtro: filtroReducer

}