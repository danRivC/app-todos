import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import * as todoActions from '../todo.actions';

@Component({
  selector: 'app-todo-page',
  templateUrl: './todo-page.component.html',
  styleUrls: ['./todo-page.component.css']
})
export class TodoPageComponent implements OnInit {

  cbxToggleAll : FormControl
  constructor(private store:Store<AppState>) { }

  ngOnInit(): void {
    this.cbxToggleAll = new FormControl(true);
  }

  toggleAll(){
    let completado: boolean;
    completado = this.cbxToggleAll.value;

    this.store.dispatch(todoActions.toggleAll({completado}))

  }

}
