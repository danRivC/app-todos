import { Component, OnInit } from '@angular/core';
import { Todo } from '../models/model';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { filtrosValidos } from 'src/app/filtro/filtro.actions';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {

  constructor(private store:Store<AppState>) { }
  todos: Todo[];
  filtroActual:filtrosValidos = 'todos';

  ngOnInit(): void {
    //this.store.select('todos').subscribe(todos=>{this.todos = todos});
    this.store.subscribe(store => {
      this.todos = store.todos;
      this.filtroActual = store.filtro;
    })

  }

}
