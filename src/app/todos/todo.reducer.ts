import { createReducer, on, State } from '@ngrx/store';
import * as actionTodo from './todo.actions';
import { Todo } from './models/model';



export const estadoInicial:Todo[] = [
    new Todo('Salvar el mundo'),
    new Todo('Vencer a Thanos'),
    new Todo('Aprender DevOps'),
    new Todo('Casarme')
];

const _todoReducer = createReducer(estadoInicial,
  on(actionTodo.crear, (state, {texto})=> [...state, new Todo(texto)]),

  on(actionTodo.eliminar, (state, {id}) => state.filter(todo => todo.id !== id)),




  on(actionTodo.toggle, (state, {id}) => {
    return state.map(todo => {
      if(todo.id === id){
        return{
          ...todo, completado: !todo.completado
        }
      }else{
        return todo;
      }
    })
  }),


  on(actionTodo.toggleAll, (state, {completado}) => {
    return state.map(todo => {
      
        return{
          ...todo, completado
        }
      
    })
  }),

  on(actionTodo.eliminarAll, (state) =>{
    return state.filter(todo=>!todo.completado)
  }),

  on(actionTodo.editar, (state, {id, texto}) => {
    return state.map(todo => {
      if(todo.id === id){
        return{
          ...todo,
          texto
        }
      }else{
        return todo;
      }
    })
  }),
)


export function todoReducer(state, action) {
  return _todoReducer(state, action);
}