import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import * as filtroActions from 'src/app/filtro/filtro.actions';
import { eliminarAll } from '../todo.actions';

@Component({
  selector: 'app-todo-footer',
  templateUrl: './todo-footer.component.html',
  styleUrls: ['./todo-footer.component.css']
})
export class TodoFooterComponent implements OnInit {

  constructor(private store:Store<AppState>) { }
  filtroActual: filtroActions.filtrosValidos = 'todos';
  filtros: filtroActions.filtrosValidos[] = [ 'todos', 'completados', 'pendientes'];
  pendientes: number = 0;
  ngOnInit(): void {
    //this.store.select('filtro').subscribe(filtro => this.filtroActual = filtro);
    this.store.subscribe(state => {
      this.filtroActual = state.filtro;
      this.pendientes = state.todos.filter(todo=> !todo.completado).length;
    })
  }
  cambiarFiltro(filtro: filtroActions.filtrosValidos){
    
    this.store.dispatch(filtroActions.setFiltro({filtro:filtro}));
  }
  elminarCompletados(){
    this.store.dispatch(eliminarAll());
  }

}
