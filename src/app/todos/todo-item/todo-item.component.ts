import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import * as todoActions from '../todo.actions';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css']
})
export class TodoItemComponent implements OnInit {

  @Input() todo;
  @ViewChild('inputFisico') txtInputFisico : ElementRef;
  cbxCompletado: FormControl;
  txtInput:FormControl;
  editando: boolean = false;
  constructor(private store: Store<AppState>) {   }

  ngOnInit(): void {
    this.cbxCompletado = new FormControl(this.todo.completado);
    this.txtInput = new FormControl(this.todo.texto, Validators.required);
    this.cbxCompletado.valueChanges.subscribe(valor => {
      this.store.dispatch(todoActions.toggle({id:this.todo.id}));
    })
  }

  editar(){
    this.editando = true;
    setTimeout(() => {
      this.txtInputFisico.nativeElement.select()
    }, 1);
    this.txtInput.setValue(this.todo.texto);
  }

  eliminar(){
    this.store.dispatch(todoActions.eliminar({id:this.todo.id}));
  }

  terminarEdicion(){
    this.editando = false;
    if(this.txtInput.value === this.todo.texto){return;}
    if(this.txtInput.valid){
      this.store.dispatch(
        todoActions.editar({
          id: this.todo.id,
          texto: this.txtInput.value
        })
      );
    }else{
      return;
    }
  }


}
